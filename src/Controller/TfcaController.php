<?php
namespace Drupal\biopama_tfca\Controller;

use Drupal\Core\Controller\ControllerBase;

/**
 * Provides route responses for the biopama_tfca module.
 */
class TfcaController extends ControllerBase {
  public function content() {
    $element = array(
	  '#theme' => 'tfca',
	  '#test_var' => $this->t('Test Value'),
    );
    return $element;
  }
}